﻿public class FlameBlast : Move
{
	private float _damage = 1.0f;

	public FlameBlast()
	{
		Name = "Flame Blast";
		MoveType = Type.Types.Fire;
	}

	public override float Action (Character user, Character target)
	{
		return target.Damage(_damage, user.Attack, MoveType);
	}
}
