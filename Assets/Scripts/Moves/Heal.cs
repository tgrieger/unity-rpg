﻿public class Heal : Move
{
	private float _healAmount = 5.0f;

	public Heal()
	{
		Name = "Heal";
		MoveType = Type.Types.Normal;
	}

	public override float Action(Character user, Character target)
	{
		return user.Heal(_healAmount);
	}
}
