﻿public class StaticSpark : Move
{
	private float _damage = 1.0f;

	public StaticSpark()
	{
		Name = "Static Spark";
		MoveType = Type.Types.Electric;
	}

	public override float Action (Character user, Character target)
	{
		return target.Damage(_damage, user.Attack, MoveType);
	}
}
