﻿public class TestAttack : Move
{
	private float _damage = 1.0f;

	public TestAttack()
	{
		Name = "TestAttack";
		MoveType = Type.Types.Normal;
	}

	public override float Action (Character user, Character target)
	{
		return target.Damage(_damage, user.Attack, MoveType);
	}
}
