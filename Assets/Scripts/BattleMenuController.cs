﻿using UnityEngine;
using System.Collections.Generic;

public class BattleMenuController : MonoBehaviour
{
	#region Constants

	private const int mainMenu = 0;
	private const int fightMenu = 1;
	private const int bagMenu = 2;

	#endregion Constants

	private MenuItem.guiDelegate currentMenu;
	private MenuItem.countDelegate currentCount;
	private MenuItem.enterDelegate currentEnter;
	private List<List<MenuItem>> menuItems = new List<List<MenuItem>>();

	private int inMenu = 0;
	private int menuItem = 0;

	// Use this for initialization
	void Start()
	{
		currentMenu = MainMenu;
		currentCount = DefaultCount;
		currentEnter = DefaultEnter;

		menuItems.Add(new List<MenuItem>());
		menuItems[mainMenu].Add(new MenuItem("Fight", FightMenu, FigthCount, FightEnter));
		menuItems[mainMenu].Add(new MenuItem("Bag", BagMenu, BagCount, BagEnter));
		menuItems[mainMenu].Add(new MenuItem("Run", MainMenu, DefaultCount, DefaultEnter));

		menuItems.Add(new List<MenuItem>());
		menuItems[fightMenu].Add(new MenuItem("Back", MainMenu, DefaultCount, DefaultEnter));

		menuItems.Add(new List<MenuItem>());
		menuItems[bagMenu].Add(new MenuItem("Back", MainMenu, DefaultCount, DefaultEnter));
	}
	
	// Update is called once per frame
	void Update()
	{
		if(Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
		{
			menuItem = menuItem + 1 > currentCount() - 1 ? 0 : menuItem + 1;
		}
		if(Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
		{
			menuItem = menuItem - 1 < 0 ? currentCount() - 1 : menuItem - 1;
		}
		
		if(Input.GetKeyDown(KeyCode.Return))
		{
			currentEnter();
		}
	}

	void OnGUI()
	{
		GUI.Window(1, new Rect(Screen.width * .7f, Screen.height * .6f, 500, 300), BattleMenu, "Battle Menu");
		GUILayout.Label("Player: " + PlayerObject.playerCharacter.Hp + "/" + PlayerObject.playerCharacter.MaxHp);
		GUILayout.Label("Enemy: " + PlayerObject.evilSphere.Hp + "/" + PlayerObject.evilSphere.MaxHp);
	}

	void BattleMenu(int windowID)
	{
		currentMenu();
	}

	void MainMenu()
	{
		inMenu = mainMenu;

		for(int i = 0; i < currentCount(); i++)
		{
			Selector (i, menuItems[inMenu][i].Label);
		}
	}

	void FightMenu()
	{
		inMenu = fightMenu;

		for(int i = 0; i < PlayerObject.playerCharacter.MoveList.Count; i++)
		{
			Selector(i, PlayerObject.playerCharacter.MoveList[i].Name);
		}

		Selector(PlayerObject.playerCharacter.MoveList.Count, menuItems[inMenu][0].Label);
	}

	void BagMenu()
	{
		inMenu = bagMenu;

		for(int i = 0; i < PlayerObject.playerCharacter.Inventory.Count; i++)
		{
			Selector(i, PlayerObject.playerCharacter.Inventory[i].ItemValue.Name + 
			         (PlayerObject.playerCharacter.Inventory[i].Count > 1 ? " x" + PlayerObject.playerCharacter.Inventory[i].Count : ""));
		}
		
		Selector(PlayerObject.playerCharacter.Inventory.Count, menuItems[inMenu][0].Label);
	}

	int DefaultCount()
	{
		return menuItems[inMenu].Count;
	}

	int BagCount()
	{
		return PlayerObject.playerCharacter.Inventory.Count + menuItems[inMenu].Count;
	}

	int FigthCount()
	{
		return PlayerObject.playerCharacter.MoveList.Count + menuItems[inMenu].Count;
	}

	void DefaultEnter()
	{
		currentMenu = menuItems[inMenu][menuItem].GuiDelegate;
		currentCount = menuItems[inMenu][menuItem].CountDelegate;
		currentEnter = menuItems[inMenu][menuItem].EnterDelegate;
		menuItem = 0;
	}

	void BagEnter()
	{
		if(menuItem < PlayerObject.playerCharacter.Inventory.Count)
		{
			PlayerObject.playerCharacter.Inventory[menuItem].ItemValue.Action(PlayerObject.playerCharacter);
			PlayerObject.evilSphere.AIBehaviour();

		}

		currentMenu = menuItems[inMenu][0].GuiDelegate;
		currentCount = menuItems[inMenu][0].CountDelegate;
		currentEnter = menuItems[inMenu][0].EnterDelegate;
		menuItem = 0;
	}

	void FightEnter()
	{
		if(menuItem < PlayerObject.playerCharacter.MoveList.Count)
		{
			if(PlayerObject.playerCharacter.MoveList[menuItem].Action(PlayerObject.playerCharacter, PlayerObject.evilSphere) <= 0)
			{
				Debug.Log("Load overworld");
				PlayerObject.playerCharacter.AddExp(1);
				Application.LoadLevel("testScene");
			}
			else
			{
				PlayerObject.evilSphere.AIBehaviour();
			}
		}

		currentMenu = menuItems[inMenu][0].GuiDelegate;
		currentCount = menuItems[inMenu][0].CountDelegate;
		currentEnter = menuItems[inMenu][0].EnterDelegate;
		menuItem = 0;
	}

	void Selector(int itemId, string label)
	{
		if(menuItem == itemId)
		{
			GUILayout.Button(label);
		}
		else
		{
			GUILayout.Label(label);
		}
	}
}
