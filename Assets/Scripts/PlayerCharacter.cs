﻿public class PlayerCharacter : Character
{
    public PlayerCharacter()
    {
        _baseHp = 10;
        _baseMana = 10;
        _baseAttack = 10;
        _baseDefense = 10;
        _baseSpeed = 10;

		_maxHp = 10;
		_curHp = 10;

		CharacterType = Type.Types.Normal;
    }

	protected override Move MoveChecker ()
	{
		if(_level == 2)
		{
			return new Heal();
		}

		return null;
	}
}
