﻿public class Type
{
	public enum Types
	{
		Normal,
		Fire,
		Water,
		Electric,
		Grass,
		Metal
	}

	public static float[,] resistantChart = 
	{
	//  N	F	W	E	G	M
		{1, 1, 1, 1, 1,	.5f}, // N
		{1, .5f, .5f, 1, 2, 2}, // F
		{1, 2, .5f, .5f, .5f, 1}, // W
		{1, 1, 2, .5f, 1, .5f}, // E
		{1, .5f, 2, .5f, 1, .5f}, // G
		{2, .5f, 1, .5f, 1, .5f} // M
	};

	public static float GetResistance(Types moveType, Types targetType)
	{
		return resistantChart[(int)moveType,(int)targetType];
	}
}
