﻿public abstract class Move
{
	public string Name;
	public Type.Types MoveType;
	public abstract float Action(Character user, Character target);
}
