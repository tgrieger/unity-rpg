﻿using UnityEngine;
using System.Collections.Generic;

public class OverworldMenuController : MonoBehaviour
{
	// TODO Get the mouse working with the menu and have it interactly nicely with the keyboard controls
	#region Constants

	private const int mainMenu = 0;
	private const int playerMenu = 1;
	private const int bagMenu = 2;

	#endregion Constants

	private int inMenu = 0;
	private int menuItem = 0;
	private List<List<MenuItem>> menuItems = new List<List<MenuItem>>();
	private MenuItem.guiDelegate currentMenu;
	private MenuItem.countDelegate currentCount;
	private MenuItem.enterDelegate currentEnter;

	public static bool paused = false;

	void Start()
	{
		menuItems.Add(new List<MenuItem>());
		menuItems[mainMenu].Add(new MenuItem("Player", PlayerMenu, DefaultCounter, DefaultEnter));
		menuItems[mainMenu].Add(new MenuItem("Bag", BagMenu, BagCounter, BagEnter));
		menuItems[mainMenu].Add(new MenuItem("Exit", UnPauseMenu, null, null));
		
		menuItems.Add(new List<MenuItem>());
		menuItems[playerMenu].Add(new MenuItem("Back", MainMenu, DefaultCounter, DefaultEnter));
		
		menuItems.Add(new List<MenuItem>());
		menuItems[bagMenu].Add(new MenuItem("Back", MainMenu, DefaultCounter, DefaultEnter));
	}

	// Update is called once per frame
	void Update()
	{
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			// Pause the game and bring up a menu or unpause and tear down the menu
			//
			paused = !paused;
			if(paused)
			{
				// Bring up the menu and stop time
				//
				Time.timeScale = 0.0f;
				inMenu = 0;
				menuItem = 0;
				currentMenu = MainMenu;
				currentCount = DefaultCounter;
				currentEnter = DefaultEnter;
			}

			else
			{
				// Tear down the menu and continue time
				//
				Time.timeScale = 1.0f;
			}
		}

		if(paused)
		{
			if(Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
			{
				menuItem = menuItem + 1 > currentCount() - 1 ? 0 : menuItem + 1;
			}
			if(Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
			{
				menuItem = menuItem - 1 < 0 ? currentCount() - 1 : menuItem - 1;
			}

			if(Input.GetKeyDown(KeyCode.Return))
			{
				currentEnter();
			}
		}
	}

	void OnGUI()
	{
		if(paused)
		{
			GUI.Window(0, new Rect(200, 200, 100, 230), PauseMenu, "test window");
		}
	}

	void PauseMenu(int windowID)
	{
		currentMenu();
	}

	void MainMenu()
	{
		inMenu = mainMenu;

		for(int i = 0; i < menuItems[inMenu].Count; i++)
		{
			Selector(i, menuItems[inMenu][i].Label);
		}
	}

	void PlayerMenu()
	{
		inMenu = playerMenu;

		GUILayout.Label("Level: " + PlayerObject.playerCharacter.Level);
		GUILayout.Label("Exp: " + PlayerObject.playerCharacter.Exp + "/" + PlayerObject.playerCharacter.MaxExp);
		GUILayout.Label("Hp: " + PlayerObject.playerCharacter.Hp);
		GUILayout.Label("Mana: " + PlayerObject.playerCharacter.Mana);
		GUILayout.Label("Attack: " + PlayerObject.playerCharacter.Attack);
		GUILayout.Label("Defense: " + PlayerObject.playerCharacter.Defense);
		GUILayout.Label("Speed: " + PlayerObject.playerCharacter.Speed);

		Selector(0, menuItems[inMenu][0].Label);
	}

	void BagMenu()
	{
		inMenu = bagMenu;

		for(int i = 0; i < PlayerObject.playerCharacter.Inventory.Count; i++)
		{
			Selector(i, PlayerObject.playerCharacter.Inventory[i].ItemValue.Name + 
			         (PlayerObject.playerCharacter.Inventory[i].Count > 1 ? " x" + PlayerObject.playerCharacter.Inventory[i].Count : ""));
		}

		Selector(PlayerObject.playerCharacter.Inventory.Count, menuItems[inMenu][0].Label);
	}

	void UnPauseMenu()
	{
		paused = false;
		Time.timeScale = 1.0f;
	}

	int DefaultCounter()
	{
		return menuItems[inMenu].Count;
	}

	int BagCounter()
	{
		return PlayerObject.playerCharacter.Inventory.Count + menuItems[inMenu].Count;
	}

	void DefaultEnter()
	{
		currentMenu = menuItems[inMenu][menuItem].GuiDelegate;
		currentCount = menuItems[inMenu][menuItem].CountDelegate;
		currentEnter = menuItems[inMenu][menuItem].EnterDelegate;
		menuItem = 0;
	}

	void BagEnter()
	{
		if(menuItem < PlayerObject.playerCharacter.Inventory.Count)
		{
			PlayerObject.playerCharacter.Inventory[menuItem].ItemValue.Action(PlayerObject.playerCharacter);
		}
		else
		{
			currentMenu = menuItems[inMenu][0].GuiDelegate;
			currentCount = menuItems[inMenu][0].CountDelegate;
			currentEnter = menuItems[inMenu][0].EnterDelegate;
			menuItem = 0;
		}
	}
	
	void Selector(int itemId, string label)
	{
		if(menuItem == itemId)
		{
			GUILayout.Button(label);
		}
		else
		{
			GUILayout.Label(label);
		}
	}
}

public class MenuItem
{
	#region Members

	private guiDelegate _guiDelegate;
	private countDelegate _countDelegate;
	private enterDelegate _enterDelegate;
	private string _label;

	#endregion Members
	
	public delegate void guiDelegate();
	public delegate int countDelegate();
	public delegate void enterDelegate();

	public MenuItem(string label, guiDelegate gD, countDelegate cD, enterDelegate eD)
	{
		GuiDelegate = gD;
		Label = label;
		CountDelegate = cD;
		EnterDelegate = eD;
	}
	
	public guiDelegate GuiDelegate
	{
		get { return _guiDelegate; }
		set { _guiDelegate = value; }
	}

	public string Label
	{
		get { return _label; }
		set { _label = value; }
	}

	public countDelegate CountDelegate
	{
		get { return _countDelegate; }
		set { _countDelegate = value; }
	}

	public enterDelegate EnterDelegate
	{
		get { return _enterDelegate; }
		set { _enterDelegate = value; }
	}
}