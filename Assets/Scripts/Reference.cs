﻿public static class Reference
{
	public const string TELEPORTER = "Teleporter";
	public const string MOUNTAIN = "Mountain";
	public const string GRASS = "Grass";
	public const string DOOR = "Door";
	public const string KEY = "Key";
	public const string NPC1 = "NPC1";

	public const string MONSTERSPAWN = "MonsterSpawn";
	public const string NOTWAlKABLE = "NotWalkable";
	public const string TESTSCENE = "testScene";
	public const string BATTLESCENE = "battleScene";
}