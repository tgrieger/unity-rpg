﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GroundSpawner : MonoBehaviour {

    public GameObject grass;
	public GameObject mountain;
	public GameObject key;
	public GameObject door;
	public GameObject teleporter;
	public GameObject npc1;
	public GameObject pavement;

	private Color building = new Color(0.8392157f, 0.8196079f, 0.7843137f);
	private Color parkingGarage = new Color(0.9686275f, 0.9372549f, 0.7176471f);
	private Color majorRoad = new Color(0.9764706f, 0.8392157f, 0.6666667f);
	private Color road = new Color(0.972549f, 0.972549f, 0.7294118f);
	private Color minorRoad = new Color(1.000f, 1.000f, 1.000f);
	private Color park = new Color(0.8117647f, 0.9254902f, 0.6588235f);
	private Color smallPark = new Color(0.8039216f, 0.9686275f, 0.7882353f);
	private Color background = new Color(0.9490196f, 0.9372549f, 0.9137255f);

	List<GameObject> terrainTiles = new List<GameObject>();
	List<List<GameObject>> mapTiles = new List<List<GameObject>>();

	// Use this for initialization
	void Start ()
    {
        Texture2D map = (Texture2D)Resources.Load("SampleMap", typeof(Texture2D));
		
		for(int y = 0; y < 100; y++)
		{
			mapTiles.Add(new List<GameObject>());
			for(int x = 0; x < 100; x++)
			{
				GameObject newSquare;
				Color color = map.GetPixel(x + 1590, y + 959);
				if(ColorCompare(color, building) || ColorCompare(color, parkingGarage))
				{
					newSquare = CreateTerrainItem(mountain, new Vector3(x - 50, y - 50, 2));
					mapTiles[y].Add(newSquare);
				}
				else if(ColorCompare(color, park) || ColorCompare(color, smallPark) || ColorCompare(color, background))
				{
					newSquare = CreateTerrainItem(grass, new Vector3(x - 50, y - 50, 2));
					mapTiles[y].Add(newSquare);
				}
				else if(ColorCompare(color, majorRoad) || ColorCompare(color, road) || ColorCompare(color, minorRoad))
				{
					newSquare = CreateTerrainItem(pavement, new Vector3(x - 50, y - 50, 2));
					mapTiles[y].Add(newSquare);
				}
				else
				{
					// Debug the color, print the colors we have and the color we got
					//
					/*Debug.Log("actual: " + color +
					          " building: " + building +
					          " parkingGarage: " + parkingGarage +
					          " park: " + park +
					          " smallPark: " + smallPark +
					          " majorRoad: " + majorRoad +
					          " road: " + road +
					          " minorRoad: " + minorRoad);*/

					mapTiles[y].Add(null);
				}
			}
		}

		CreateMap();
	}

	void Update()
	{
		if(Input.GetKey(KeyCode.R))
	   	{
			Dispose();
			Start();
		}
	}

	void Dispose()
	{
		foreach (GameObject terrainTile in terrainTiles)
		{
			Destroy(terrainTile);
		}
		terrainTiles.Clear ();
	}

	GameObject CreateTerrainItem(GameObject type, Vector3 position)
	{
		GameObject newTerrainItem = (GameObject) Instantiate(type, position, Quaternion.identity);
		terrainTiles.Add (newTerrainItem);
		return newTerrainItem;
	}

	void CreateMap()
	{
		for(int y = 0; y < mapTiles.Count; y++)
		{
			for(int x = 0; x < mapTiles[y].Count; x++)
			{
				GameObject newTile = mapTiles[y][x];
				if(newTile == null)
				{
					for(int positionOffset = 1; mapTiles[y][x] == null && positionOffset < 4; positionOffset++)
					{
						if(x - positionOffset >= 0 && mapTiles[y][x - positionOffset] != null)
						{
							mapTiles[y][x] = CreateTerrainItem(mapTiles[y][x-positionOffset], new Vector3(x - 50, y - 50, 2));
						}
					}
					for(int positionOffset = 1; mapTiles[y][x] == null && positionOffset < 4; positionOffset++)
					{
						if(y - positionOffset >= 0 && mapTiles[y - positionOffset][x] != null)
						{
							mapTiles[y][x] = CreateTerrainItem(mapTiles[y-positionOffset][x], new Vector3(x - 50, y - 50, 2));
						}
					}
				}
			}
		}
	}

	bool ColorCompare(Color color1, Color color2, float range = .0001f)
	{
		return Mathf.Abs(color1.r - color2.r) < range && Mathf.Abs(color1.g - color2.g) < range && Mathf.Abs(color1.b - color2.b) < range;
	}
}
