﻿public abstract class Item
{
	public string Name;
	public abstract void Action(Character user);
}
