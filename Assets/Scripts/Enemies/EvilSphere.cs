﻿using UnityEngine;
using System.Collections;

public class EvilSphere : Character
{
	TestAttack testAttack;

	public EvilSphere()
	{
		_name = "Evil Sphere";
		_baseHp = 10;
		_baseMana = 10;
		Attack = 2;
		_baseDefense = 10;
		_baseSpeed = 10;
		
		_maxHp = 10;
		_curHp = 10;

		testAttack = new TestAttack();
		CharacterType = Type.Types.Metal;
	}

	public override void AIBehaviour ()
	{
		testAttack.Action(this, PlayerObject.playerCharacter);
	}
}
