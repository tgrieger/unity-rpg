﻿using UnityEngine;
using System.Collections.Generic;

public class PlayerObject : MonoBehaviour
{

	public static PlayerCharacter playerCharacter = new PlayerCharacter();
	public static EvilSphere evilSphere;

	// Use this for initialization
	void Start ()
	{
		if(playerCharacter.Inventory == null)
		{
			playerCharacter.Inventory = new List<ItemStack>();
			playerCharacter.AddItem(new Potion(), 2);
			playerCharacter.MoveList = new List<Move>();
			playerCharacter.MoveList.Add(new TestAttack());
			playerCharacter.MoveList.Add(new FlameBlast());
		}

		evilSphere = new EvilSphere();
	}
}
