﻿using System.Collections.Generic;

public abstract class Character
{
    #region Members

    protected int _level = 1;
    protected int _maxLevel = 100;

    protected float _curExp = 0.0f;
    protected float _maxExp = 1.0f;

	protected float _curHp = 1.0f;
    protected float _baseHp = 1.0f;
	protected float _maxHp = 1.0f;

	protected float _curMana = 1.0f;
    protected float _baseMana = 1.0f;
	protected float _maxMana = 1.0f;

	protected float _curAttack = 1.0f;
	protected float _baseAttack = 1.0f;
    protected float _attack = 1.0f;

	protected float _curDefense = 1.0f;
	protected float _baseDefense = 1.0f;
    protected float _defense = 1.0f;

	protected float _curSpeed = 1.0f;
	protected float _baseSpeed = 1.0f;
    protected float _speed = 1.0f;

	protected CharacterState _state = CharacterState.Alive;

	protected List<ItemStack> _inventory;
	protected List<Move> _moveList;

	protected string _name = "empty";

    #endregion Members

    #region Properties

    public enum CharacterState
	{
		Alive,
		Dead
	}

    public int Level
    {
        get { return _level; }
    }

    public int MaxLevel
    {
        get { return _maxLevel; }
    }

    public float Exp
    {
        get { return _curExp; }
        set
        {
            if (value >= _maxExp)
            {
                // TODO: call a levelup function here.
                // LevleUp will update all of the maxValues based on the base values and level of the character
                //
                _curExp = value - _maxExp;
                LevelUp();
            }
            else
            {
                _curExp = value;
            }
        }
    }

    public float MaxExp
    {
        get { return _maxExp; }
    }

    public float Hp
    {
        get { return _curHp; }
        set
        {
            if (value > _maxHp)
            {
                _curHp = _maxHp;
            }
            else
            {
                _curHp = value;
            }
        }
    }

    public float BaseHp
    {
        get { return _baseHp; }
    }

    // TODO: instead of returning just _maxHp, return a value determined by a formula that increases based on level
    //
    public float MaxHp
    {
        get { return _maxHp; }
    }

    public float Mana
    {
        get { return _curMana; }
        set
        {
            if (value > _maxMana)
            {
                _curMana = MaxMana;
            }
            else
            {
                _curMana = value;
            }
        }
    }

    public float BaseMana
    {
        get { return _baseMana; }
    }

    // TODO: instead of returning just _maxMana, return a value determined by a formula that increases based on level
    // same for all below
    //
    public float MaxMana
    {
        get { return _maxMana; }
    }

    public float Attack
    {
        get { return _curAttack; }
        set { _curAttack = value; }
    }

    public float BaseAttack
    {
        get { return _baseAttack; }
    }

    public float Defense
    {
        get { return _curDefense; }
        set { _curDefense = value; }
    }

    public float BaseDefense
    {
        get { return _baseDefense; }
    }

    public float Speed
    {
        get { return _curSpeed; }
        set { _curSpeed = value; }
    }

    public float BaseSpeed
    {
        get { return _baseSpeed; }
    }

    public CharacterState State
    {
        get { return _state; }
        set { _state = value; }
    }

	public List<ItemStack> Inventory
	{
		get { return _inventory; }
		set { _inventory = value; }
	}

	public List<Move> MoveList
	{
		get { return _moveList; }
		set { _moveList = value; }
	}

	public string Name
	{
		get { return _name; }
		set { _name = value; }
	}

	public Type.Types CharacterType
	{
		get; set;
	}

    #endregion Properties

    #region Methods

    #region Utilities

    public float Heal(float health)
	{
        return Hp += health;
	}

	public float Damage(float rawDamage, float attack, Type.Types damageType)
	{
		float damage = rawDamage * Type.GetResistance(damageType, CharacterType) * attack / Defense;
		Hp -= damage;
		if(Hp <= 0)
		{
			State = CharacterState.Dead;
			Hp = 0;
		}

		return Hp;
	}

    public float AddExp(float exp)
    {
        return Exp += exp;
    }

	public int HasItem(string itemName)
	{
		foreach(ItemStack itemStack in Inventory)
		{
			if(itemStack.ItemValue.Name.Equals(itemName))
			{
				return itemStack.Count;
			}
		}

		return 0;
	}

	public void AddItem(Item item)
	{
		AddItem(item, 1);
	}

	public int AddItem(Item item, int count)
	{
		foreach(ItemStack itemStack in Inventory)
		{
			if(itemStack.ItemValue.Name.Equals(item.Name))
			{
				itemStack.Count += count;
				return itemStack.Count;
			}
		}

		Inventory.Add(new ItemStack(item, count));
		return count;
	}

	public int RemoveItem(Item item)
	{
		return RemoveItem (item, 1);
	}

	public int RemoveItem(string itemName)
	{
		return RemoveItem(itemName, 1);
	}

	public int RemoveItem(Item item, int count)
	{
		foreach(ItemStack itemStack in Inventory)
		{
			if(itemStack.ItemValue == item)
			{
				itemStack.Count -= count;
				if(itemStack.Count <= 0)
				{
					Inventory.Remove(itemStack);
					return 0;
				}

				return itemStack.Count;
			}
		}

		// item did not exist in list
		//
		return -1;
	}

	public int RemoveItem(string itemName, int count)
	{
		foreach(ItemStack itemStack in Inventory)
		{
			if(itemStack.ItemValue.Name.Equals(itemName))
			{
				itemStack.Count -= count;
				if(itemStack.Count <= 0)
				{
					Inventory.Remove(itemStack);
					return 0;
				}
				
				return itemStack.Count;
			}
		}
		
		// item did not exist in list
		//
		return -1;
	}

	public virtual void AIBehaviour(){}

    #endregion Utilities

    #region Updaters

    protected virtual void LevelUp()
    {
        _level++;
        _maxExp += 1;

        // Do each stat individually so we can override the way each stat updates without having to override the whole LevelUp method
        UpdateMaxHp();
        UpdateMaxMana();
        UpdateAttack();
        UpdateDefense();
        UpdateSpeed();
		Move temp = MoveChecker();
		if(temp != null)
		{
			MoveList.Add(temp);
		}
    }

    protected virtual void UpdateMaxHp()
    {
        float percentage = _curHp / _maxHp;
        _maxHp = _baseHp * _level;
        _curHp = percentage * _maxHp;
    }

    protected virtual void UpdateMaxMana()
    {
        float percentage = _curMana / _maxMana;
        _maxMana = _baseMana * _level;
        _curMana = percentage * _maxMana;
    }

    protected virtual void UpdateAttack()
    {
        float oldAttack = _attack;
        _attack = _baseAttack * _level;

        // Do this in case you level in the middle of a fight and your attack is augmented
        //
        _curAttack += _attack - oldAttack;
    }

    protected virtual void UpdateDefense()
    {
        float oldDefense = _defense;
        _defense = _baseDefense * _level;

        // Do this in case you level in the middle of a fight and your defense is augmented
        //
        _curDefense += _defense - oldDefense;
    }

    protected virtual void UpdateSpeed()
    {
        float oldSpeed = _speed;
        _speed = _baseSpeed * _level;

        // Do this in case you level in the middle of a fight and your defense is augmented
        //
        _curSpeed += _speed - oldSpeed;
    }

	protected virtual Move MoveChecker()
	{
		return null;
	}

    #endregion Updaters

    #endregion Methods
}