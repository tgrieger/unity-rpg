﻿using UnityEngine;
using System.Collections.Generic;

public class PlayerController : MonoBehaviour
{
	private const string Direction = "Direction";
	private const string Horizontal = "Horizontal";
	private const string Vertical = "Vertical";
	private float vertical;
	private float horizontal;
	private const int numDirections = 4;
	private Animator anim;
	private bool wall = false;
	private Directionality playerDirection;
	private bool allCollisions = false;

	public float speed = 1.0f;
	public float slowSpeed = 1.0f;

	enum Directionality
	{
		North,
		East,
		South,
		West,
		NorthIdle,
		EastIdle,
		SouthIdle,
		WestIdle
	}

	// Use this for initialization
	void Start ()
	{
		anim = this.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		wall = allCollisions;
		allCollisions = false;

		anim.speed = wall ? slowSpeed : speed;

		if(Input.GetKey(KeyCode.W))
		{
			vertical = 1;
			setDirection(Directionality.North);
		}
		else if(Input.GetKey(KeyCode.D))
		{
			horizontal = 1;
			setDirection(Directionality.East);
		}
		else if(Input.GetKey(KeyCode.S))
		{
			vertical = -1;
			setDirection(Directionality.South);
		}
		else if(Input.GetKey(KeyCode.A))
		{
			horizontal = -1;
			setDirection(Directionality.West);
		}
		else if(anim.GetInteger(Direction) < numDirections)
		{
			setDirection(anim.GetInteger(Direction) + numDirections);
		}

		Camera.main.transform.position = new Vector3(transform.position.x, transform.position.y, -10);
	}

	void OnTriggerEnter2D(Collider2D collider2D)
	{
		if(collider2D.tag.Equals(Reference.MONSTERSPAWN))
		{
			if(Random.value <= .05)
			{
				Debug.Log("Load battle scene");
				//Application.LoadLevel(Reference.BATTLESCENE);
			}
		}

		if(collider2D.tag.Equals(Reference.KEY))
		{
			Destroy(collider2D.gameObject);
			PlayerObject.playerCharacter.AddItem(new Key());
		}
	}

	void OnTriggerStay2D(Collider2D collider2D)
	{
		if(collider2D.tag.Equals(Reference.TELEPORTER) && GetDistance(collider2D.gameObject) < .2f)
		{
			Application.LoadLevel(Reference.TESTSCENE);
		}
	}

	private void setDirection(Directionality direction)
	{
		playerDirection = direction;
		setDirection((int)direction);
		if(!wall)
		{
			if(direction == Directionality.East || direction == Directionality.West)
			{
				transform.position += transform.right * horizontal * (wall ? slowSpeed : speed) * Time.deltaTime;
			}
			else if(direction == Directionality.North || direction == Directionality.South)
			{
				transform.position += transform.up * vertical * (wall ? slowSpeed : speed) * Time.deltaTime;
			}
		}
	}

	private void setDirection(int direction)
	{
		anim.SetInteger(Direction, direction);
	}

	void OnCollisionEnter2D(Collision2D collision)
	{
		if(collision.gameObject.tag.Equals(Reference.DOOR))
		{
			if(PlayerObject.playerCharacter.HasItem(Reference.KEY) > 0)
			{
				Destroy(collision.gameObject);
				PlayerObject.playerCharacter.RemoveItem(Reference.KEY);
			}
		}

		if(collision.gameObject.tag.Equals(Reference.NOTWAlKABLE) || collision.gameObject.tag.Equals(Reference.NPC1))
		{
			wall = true;
		}
	}

	void OnCollisionStay2D(Collision2D collision)
	{
		foreach(ContactPoint2D contact in collision.contacts)
		{
			if((playerDirection == Directionality.North && contact.normal.y == -1) ||
			   (playerDirection == Directionality.East && contact.normal.x == -1) ||
			   (playerDirection == Directionality.South && contact.normal.y == 1) ||
			   (playerDirection == Directionality.West && contact.normal.x == 1))
			{
				allCollisions = true;
				break;
			}
		}
	}

	void OnCollisionExit2D(Collision2D collision)
	{
		wall = false;
	}

	private float GetDistance(GameObject gameObj)
	{
		float xSquared = Mathf.Pow(transform.position.x - gameObj.transform.position.x, 2);
		float ySquared = Mathf.Pow(transform.position.y - gameObj.transform.position.y, 2);
		return Mathf.Sqrt(xSquared + ySquared);
	}
}
