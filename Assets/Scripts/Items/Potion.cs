﻿public class Potion : Item
{
	public Potion()
	{
		Name = "Potion";
	}

	public override void Action(Character user)
	{
		user.Heal(20);
		user.RemoveItem(this);
	}
}
