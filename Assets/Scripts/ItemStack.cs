﻿public class ItemStack
{
	private Item _item;
	private int _count;

	public ItemStack(Item item)
	{
		this._item = item;
		this._count = 1;
	}

	public ItemStack(Item item, int count)
	{
		this._item = item;
		this._count = count;
	}

	public Item ItemValue
	{
		get { return _item; }
		set { _item = value; }
	}

	public int Count
	{
		get { return _count; }
		set { _count = value; }
	}
}
